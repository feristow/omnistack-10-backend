const axios = require('axios');
const Dev = require('../models/Dev');
const parseStringAsArray = require('../utils/parseStringAsArray');
const { findConnections, sendMessage } = require('../websocket');

module.exports = {
  async index (req, res) {
    const devs = await Dev.find();
    return res.json(devs);
  }, 
  async store (req, res) {
    /** Recupera os dados do body */
    const { github_username, techs, latitude, longitude } = req.body;
    /** Valida se o user já existe na base */
    let dev = await Dev.findOne({ github_username });
    if (!dev) {
      /** Faz a requisição pra API do GITHUB para recuperar os dados do modelo de DEV */
      const response = await axios.get(`https://api.github.com/users/${github_username}`);
      /** Usa a desestruturação de dados para setar os dados */
      const { name = login, avatar_url, bio } = response.data;
      /** Quebra a string de techs em array e remove os espaços */
      const techsArray = parseStringAsArray(techs);
      /** Configuraçaõ da geolocalização */
      const location = {
        type: 'Point',
        coordinates: [longitude, latitude]
      };
      /** Realiza a criação do usuário */
      dev = await Dev.create({
        github_username,
        name,
        avatar_url,
        bio,
        techs: techsArray,
        location
      });
      /** Envia a informação para o mobile sem precisar realizar uma nova requisição */
      /** Filtrar as conexões e procurar aquelas que satisfaçam as condições de pelo menos 1 das techs + geolocation há 10 km de distância */
      const sendSocketMessageTo = findConnections(
        { latitude, longitude },
        techsArray
      );
      sendMessage(sendSocketMessageTo, 'new-dev', dev);
    }

    /** Retorno do cadastro para o front */
    return res.json(dev);
  },
  async update (req, res) {
    /** Recupera o ID do dev */
    const { id } = req.params;
    /** Usa a desestruturação para setar as variáveis */
    const { name, bio, avatar_url, techs, latitude, longitude } = req.body;
    /** Quebra a string de techs em array e remove os espaços */
    const techsArray = parseStringAsArray(techs);
    /** Configuraçaõ da geolocalização */
    const location = {
      type: 'Point',
      coordinates: [longitude, latitude]
    };
    /** Realiza a atualização do usuário */
    dev = await Dev.findByIdAndUpdate(id, {
      $set: {
        name,
        bio,
        avatar_url,
        techs: techsArray,
        location
      }
    });
    /** Retorno com a atualização para o front */
    return res.json(dev);
  },
  async destroy (req, res) {
    /** Recupera o ID do dev */
    const { id } = req.params;
    /** Realiza o delete do usuário */
    Dev.findByIdAndDelete(id, () => console.log('Sucesso ao deletar usuário'));
    /** Retorno com a atualização para o front */
    return res.json({ message: 'Dev deletado com sucesso.'});
  }
};