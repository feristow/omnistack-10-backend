const axios = require('axios');
const Dev = require('../models/Dev');
const parseStringAsArray = require('../utils/parseStringAsArray');

module.exports = {
  /**
   * Buscar todos os devs em um raio de 10km e filtrar por tecnologias
   */
  async index (req, res) {
    const { latitude, longitude, techs } = req.query;
    const techsArray = parseStringAsArray(techs);
    const devs = await Dev.find({
      /** Filtro de tecnologias */
      techs: {
        $in: techsArray
      },
      /** Filtro para latitude/longitude mongodb */
      location: {
        $near: {
          $geometry: {
            type: 'Point',
            coordinates: [longitude, latitude]
          },
          $maxDistance: 10000
        }
      }
    }); 
    return res.json({ devs });
  }
};