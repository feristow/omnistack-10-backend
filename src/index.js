const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http');
const mongoose = require('mongoose');
const routes = require('./routes');
const { setupWebsocket } = require('./websocket');

/* Instancia as funcionalidades de um server */
const app = express();
const server = http.Server(app);

setupWebsocket(server);

/* Instancia a conexão com o banco de dados */
mongoose.connect('mongodb+srv://feristow:o1V0NCMTzWyH4gW4@cluster0-zgaah.mongodb.net/omnistack?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

/* .use(): Informa que toda a aplicação irá usar essa configuração */
app.use(cors({ origin: 'http://localhost:3000' }));
/* Essa configuração refere-se para converter o body recebido via req */
app.use(bodyParser.json());
/* Arquivo de configuração das rotas */
app.use(routes);

server.listen(3333, () => console.log('Aplicação iniciada na porta 3333.'));