const { Router } = require('express');
const routes = Router();
const DevController = require('./controllers/DevController');
const SearchController = require('./controllers/SearchController');

/* Métodos HTTP: get, post, put e delete */

/**
 * Tipos de parâmetros
 * Query params: req.query => Filtros, ordenação, paginação, etc
 * Route params: req.params => Identificar recurso de alteração/remoção
 * Body: req.body => Dados para criação ou alteração de um registro
 * ---------------------------------------------------------------------
 * Nome de funções básicas (5)
 * 1: Index,
 * 2: Show,
 * 3: Store,
 * 4: Update,
 * 5: Destroy
 */
routes.get('/devs', DevController.index);
routes.post('/devs', DevController.store);
routes.put('/devs/:id', DevController.update);
routes.delete('/devs/:id', DevController.destroy);

routes.get('/search', SearchController.index)

// routes.post('/', (req, res) => {

// });

// routes.put('/', (req, res) => {

// });

// routes.delete('/', (req, res) => {

// });

module.exports = routes;